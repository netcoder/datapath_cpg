#include <stdio.h>
#include <ev.h>

#include "main.h"

static int *rfd = &pfd[0][0];
static int *wfd = &pfd[1][1];

static void pipe_cb(EV_P_ ev_io *w, int revents)
{
	printf("OOOOOOOOOO\n");
}

static void cpg_cb(EV_P_ ev_io *w, int revents)
{
	dispatch_cpg();
}

static void quorum_cb(EV_P_ ev_io *w, int revents)
{
	dispatch_quorum();
}

static void cpg_cfg_cb(EV_P_ ev_io *w, int revents)
{
	dispatch_cfg();
}

void do_daemon(void)
{
	ev_io pipe_watcher, cpg_watcher, cpg_cfg_watcher, quorum_watcher;
	struct ev_loop *loop = EV_DEFAULT;
	int fd;

	fd = setup_quorum();
	if (fd == -1) {
		printf("Couldn't initialize CFG %d\n", fd);
		return;
	}

	ev_io_init(&quorum_watcher, quorum_cb, fd, EV_READ);
	ev_io_start(loop, &quorum_watcher);

	fd = setup_cfg();
	if (fd == -1) {
		printf("Couldn't initialize CFG %d\n", fd);
		return;
	}

	ev_io_init(&cpg_cfg_watcher, cpg_cfg_cb, fd, EV_READ);
	ev_io_start(loop, &cpg_watcher);

	fd = setup_cpg();
	if (fd == -1) {
		printf("Couldn't initialize CFG %d\n", fd);
		return;
	}

	ev_io_init(&cpg_watcher, cpg_cb, fd, EV_READ);
	ev_io_start(loop, &cpg_watcher);

	ev_io_init(&pipe_watcher, pipe_cb, *rfd, EV_READ);
	ev_io_start(loop, &pipe_watcher);

	ev_run(loop, 0);
#if 0
	cpg_finalize(handle);
	corosync_cfg_finalize(ch);
	quorum_finalize(qh);
#endif
}

