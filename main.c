#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <ev.h>

#include "main.h"

int our_nodeid = -1;
int pfd[2][2];

static void do_parent()
{
	do_daemon();
}

static void do_child()
{
	do_app();
}

static void sigint_cb(struct ev_loop *loop, ev_signal *w, int revents)
{
	ev_break(loop, EVBREAK_ALL);
}

int main(int argc, char **argv)
{
	ev_signal exitsig;
	int pid, rv;

	rv = pipe(pfd[0]);
	if (rv == -1)
		return 1;

	rv = pipe(pfd[1]);
	if (rv == -1)
		return 1;

	ev_signal_init(&exitsig, sigint_cb, SIGINT);
	ev_signal_start(EV_DEFAULT, &exitsig);

	pid = fork();
	if (pid == 0) {
		close(pfd[0][0]);
		close(pfd[1][1]);
		ev_loop_fork(EV_DEFAULT);
		do_child();
		return 0;
	}

	close(pfd[0][1]);
	close(pfd[1][0]);

	do_parent();
	wait(NULL);

	return 0;
}
