#include <unistd.h>
#include <stdio.h>
#include <ev.h>

#include "main.h"

static int *rfd = &pfd[1][0];
static int *wfd = &pfd[0][1];

static void pipe_cb(EV_P_ ev_io *w, int revents)
{
	unsigned char buf[4096];

	printf("FOOOOOOO\n");
	read(*rfd, &buf, 4096);
#if 0
	unsigned char str[32];

	read(*rfd, &str, 32);
	printf("%s\n", str);
#endif
}

void do_app(void)
{
	struct ev_loop *loop = EV_DEFAULT;
	ev_io pipe_watcher;

	ev_io_init(&pipe_watcher, pipe_cb, *rfd, EV_READ);
	ev_io_start(loop, &pipe_watcher);

	ev_loop(loop, 0);
}
