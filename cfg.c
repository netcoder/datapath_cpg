#include <corosync/cfg.h>
#include <unistd.h>
#include <stdio.h>
#include <ev.h>

#include "main.h"

static corosync_cfg_handle_t ch;

static void shutdown_callback(corosync_cfg_handle_t h,
			      corosync_cfg_shutdown_flags_t flags)
{
	corosync_cfg_replyto_shutdown(ch, COROSYNC_CFG_SHUTDOWN_FLAG_YES);
}

static corosync_cfg_callbacks_t cfg_callbacks =
{
	.corosync_cfg_shutdown_callback = shutdown_callback,
};

int setup_cfg(void)
{
	cs_error_t err;
	unsigned int nodeid;
	int fd;
	int retry = 1;

try_again:
	err = corosync_cfg_initialize(&ch, &cfg_callbacks);
	if (err != CS_OK) {
		if ((err == CS_ERR_TRY_AGAIN) && (retry <= 10)) {
			printf("corosync has not completed initialization.. retry %d\n", retry);
			sleep(1);
			retry++;
			goto try_again;
		}
		printf("corosync cfg init error %d\n", err);
		return -1;
	}

	err = corosync_cfg_fd_get(ch, &fd);
	if (err != CS_OK) {
		printf("corosync cfg fd_get error %d\n", err);
		corosync_cfg_finalize(ch);
		return -1;
	}

	err = corosync_cfg_local_get(ch, &nodeid);
	if (err != CS_OK) {
		printf("corosync cfg local_get error %d=b\n", err);
		corosync_cfg_finalize(ch);
		return -1;
	}

	our_nodeid = nodeid;
	printf("our_nodeid %d\n", our_nodeid);

	if (our_nodeid < 0) {
		printf("negative nodeid, set corosync totem.clear_node_high_bit\n");
		corosync_cfg_finalize(ch);
		return -1;
	}

	return fd;
}

void dispatch_cfg(void)
{
	cs_error_t err;

	err = corosync_cfg_dispatch(ch, CS_DISPATCH_ALL);
	if (err != CS_OK)
		printf("process_cluster_cfg cfg_dispatch %d", err);
}
