#include <corosync/cpg.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <ev.h>

#include "main.h"

static cpg_handle_t handle;

static struct cpg_name group_name = {
	.value = "cpg_datapath",
	.length = 6
};

void dispatch_cpg(void)
{
	cpg_dispatch(handle, CS_DISPATCH_ALL);
}

/* stolen from dlm_controld */
void log_config(const struct cpg_name *group_name,
		const struct cpg_address *member_list,
		size_t member_list_entries,
		const struct cpg_address *left_list,
		size_t left_list_entries,
		const struct cpg_address *joined_list,
		size_t joined_list_entries)
{
	char m_buf[128];
	char j_buf[32];
	char l_buf[32];
	size_t i, len, pos;
	int ret;

	memset(m_buf, 0, sizeof(m_buf));
	memset(j_buf, 0, sizeof(j_buf));
	memset(l_buf, 0, sizeof(l_buf));

	len = sizeof(m_buf);
	pos = 0;
	for (i = 0; i < member_list_entries; i++) {
		ret = snprintf(m_buf + pos, len - pos, " %d",
			       member_list[i].nodeid);
		if (ret >= len - pos)
			break;
		pos += ret;
	}

	len = sizeof(j_buf);
	pos = 0;
	for (i = 0; i < joined_list_entries; i++) {
		ret = snprintf(j_buf + pos, len - pos, " %d",
			       joined_list[i].nodeid);
		if (ret >= len - pos)
			break;
		pos += ret;
	}

	len = sizeof(l_buf);
	pos = 0;
	for (i = 0; i < left_list_entries; i++) {
		ret = snprintf(l_buf + pos, len - pos, " %d",
			       left_list[i].nodeid);
		if (ret >= len - pos)
			break;
		pos += ret;
	}

	printf("%s conf %zu %zu %zu memb%s join%s left%s\n", group_name->value,
	       member_list_entries, joined_list_entries, left_list_entries,
	       strlen(m_buf) ? m_buf : " 0", strlen(j_buf) ? j_buf : " 0",
	       strlen(l_buf) ? l_buf : " 0");
}

static void cpg_deliver_fn(cpg_handle_t handle,
			   const struct cpg_name *group_name,
			   uint32_t nodeid,
			   uint32_t pid,
			   void *m,
			   size_t msg_len)
{
	const struct cpg_address *member_list;
	size_t *member_list_entries;
	const struct cpg_address *left_list;
	size_t *left_list_entries;
	const struct cpg_address *joined_list;
	size_t *joined_list_entries;
	size_t cur = 0;

	member_list_entries = m + cur;
	cur += sizeof(*member_list_entries);
	member_list = m + cur;
	cur += (sizeof(*member_list) * *member_list_entries);

	left_list_entries = m + cur;
	cur += sizeof(*left_list_entries);
	left_list = m + cur;
	cur += (sizeof(*left_list) * *left_list_entries);

	joined_list_entries = m + cur;
	cur += sizeof(*joined_list_entries);
	joined_list = m + cur;
	cur += (sizeof(*joined_list) * *joined_list_entries);

	//write(*wfd, m, msg_len);

	printf("nodeid %u\n", nodeid);
	log_config(group_name, member_list, *member_list_entries,
		   left_list, *left_list_entries, joined_list, *joined_list_entries);
}

static void cpg_confchg_fn(cpg_handle_t handle,
			   const struct cpg_name *group_name,
			   const struct cpg_address *member_list, size_t member_list_entries,
			   const struct cpg_address *left_list, size_t left_list_entries,
			   const struct cpg_address *joined_list, size_t joined_list_entries)
{
	size_t bufsz = 0, cur = 0;
	struct iovec iov = {};
	void *buf;

	bufsz += (sizeof(member_list_entries));
	bufsz += (sizeof(*member_list) * member_list_entries);

	bufsz += (sizeof(left_list_entries));
	bufsz += (sizeof(*left_list) * left_list_entries);

	bufsz += (sizeof(joined_list_entries));
	bufsz += (sizeof(*joined_list) * joined_list_entries);

	buf = calloc(1, bufsz);
	if (!buf)
		return;

	memcpy(buf + cur, &member_list_entries, sizeof(member_list_entries));
	cur += sizeof(member_list_entries);
	memcpy(buf + cur, member_list, (sizeof(*member_list) * member_list_entries));
	cur += (sizeof(*member_list) * member_list_entries);

	memcpy(buf + cur, &left_list_entries, sizeof(left_list_entries));
	cur += sizeof(left_list_entries);
	memcpy(buf + cur, left_list, (sizeof(*left_list) * left_list_entries));
	cur += (sizeof(*left_list) * left_list_entries);

	memcpy(buf + cur, &joined_list_entries, sizeof(joined_list_entries));
	cur += sizeof(joined_list_entries);
	memcpy(buf + cur, joined_list, (sizeof(*joined_list) * joined_list_entries));
	cur += (sizeof(*joined_list) * joined_list_entries);

	iov.iov_base = buf;
	iov.iov_len = bufsz;

	cpg_mcast_joined(handle, CPG_TYPE_AGREED, &iov, 1);
	free(buf);
}

static cpg_callbacks_t callbacks = {
	cpg_deliver_fn,
	cpg_confchg_fn
};

int setup_cpg(void)
{
	cs_error_t result;
	int fd;

	result = cpg_initialize(&handle, &callbacks);
	if (result != CS_OK) {
		printf("Couldn't initialize CPG service %d\n", result);
		return -1;
	}

	result = cpg_join(handle, &group_name);
	if (result != CS_OK) {
		printf("cpg_join failed with result %d\n", result);
		return -1;
	}

	cpg_fd_get(handle, &fd);

	return fd;
}

