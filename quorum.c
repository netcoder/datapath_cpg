#include <corosync/quorum.h>
#include <stdio.h>
#include <ev.h>

#include "main.h"

static uint32_t leavejoin_nodes[MAX_NODES];
static int leavejoin_count;
static quorum_handle_t qh;

static void quorum_callback(quorum_handle_t cbhandle, uint32_t quorate,
			    struct quorum_ring_id ring_id, uint32_t node_list_entries,
			    const uint32_t *node_list)
{
	printf("foo\n");
}

static int is_member(uint32_t *node_list, int count, uint32_t nodeid)
{
	int i;

	for (i = 0; i < count; i++) {
		if (node_list[i] == nodeid)
			return 1;
	}
	return 0;
}

static int is_leavejoin_node(uint32_t nodeid)
{
	return is_member(leavejoin_nodes, leavejoin_count, nodeid);
}

static void quorum_nodelist_callback(quorum_handle_t cbhandle, struct quorum_ring_id ring_id,
				     uint32_t member_list_entries, const uint32_t *member_list,
				     uint32_t joined_list_entries, const uint32_t *joined_list,
				     uint32_t left_list_entries, const uint32_t *left_list)
{
	uint64_t ring_seq = ring_id.seq;
	int i, j;

	for (i = 0; i < left_list_entries; i++) {
		printf("cluster left_list %u seq %llu\n",
		       left_list[i], (unsigned long long)ring_seq);
	}

	for (j = 0; j < joined_list_entries; j++) {
		printf("cluster joined_list %u seq %llu\n",
		       joined_list[j], (unsigned long long)ring_seq);
	}

	for (i = 0; i < left_list_entries; i++) {
		for (j = 0; j < joined_list_entries; j++) {
			if (joined_list[j] == left_list[i]) {
				printf("cluster node %d left and joined\n", joined_list[j]);
				if (!is_leavejoin_node(joined_list[j]))
					leavejoin_nodes[leavejoin_count++] = joined_list[j];
			}
		}
	}
}

void dispatch_quorum(void)
{
	cs_error_t err;

	err = quorum_dispatch(qh, CS_DISPATCH_ONE);
	if (err != CS_OK)
		printf("update_cluster quorum_dispatch %d\n", err);
}

int setup_quorum(void)
{
	quorum_model_v1_data_t model_data;
	uint32_t quorum_type = 0;
	cs_error_t err;
	int fd;

	memset(&model_data, 0, sizeof(model_data));
	model_data.quorum_notify_fn = quorum_callback;
	model_data.nodelist_notify_fn = quorum_nodelist_callback;

	err = quorum_model_initialize(&qh, QUORUM_MODEL_V1, (quorum_model_data_t *)&model_data, &quorum_type, NULL);
	if (err != CS_OK) {
		printf("quorum init error %d\n", err);
		return -1;
	}

	err = quorum_fd_get(qh, &fd);
	if (err != CS_OK) {
		printf("quorum fd_get error %d\n", err);
		goto fail;
	}

	err = quorum_trackstart(qh, CS_TRACK_CHANGES);
	if (err != CS_OK) {
		printf("quorum trackstart error %d\n", err);
		goto fail;
	}

	return fd;
 fail:
	quorum_finalize(qh);
	return -1;
}
