#ifndef __MAIN_H__
#define __MAIN_H__

#define MAX_NODES 32

void do_daemon(void);
void do_app(void);
int setup_cfg(void);
int setup_quorum(void);
int setup_cpg(void);

void dispatch_cpg(void);
void dispatch_quorum(void);
void dispatch_cfg(void);

extern int our_nodeid;
extern int pfd[2][2];

#endif /* __MAIN_H__ */
